--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: auth_user; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE auth_user (
    id integer NOT NULL,
    first_name character varying(128),
    last_name character varying(128),
    email character varying(128),
    password character varying(512),
    reset_password_key character varying(512),
    cedula character varying(50),
    tel_cel character varying(50)
);


ALTER TABLE auth_user OWNER TO postgres;

--
-- Name: casas_fotos; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE casas_fotos (
    id integer NOT NULL,
    archivo character varying(100),
    id_casa integer
);


ALTER TABLE casas_fotos OWNER TO postgres;

--
-- Name: casas_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE casas_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE casas_id_seq OWNER TO postgres;

--
-- Name: casas_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE casas_id_seq OWNED BY casas_fotos.id;


--
-- Name: casas_info; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE casas_info (
    id integer NOT NULL,
    nombre_carpeta character varying(100),
    habitacion integer,
    banos integer,
    estacionamiento boolean,
    area_verde boolean,
    cocina boolean,
    documentos boolean,
    descripcion text,
    id_user integer,
    vendida boolean DEFAULT false,
    tipo integer,
    estado character varying(150),
    zona character varying(250)
);


ALTER TABLE casas_info OWNER TO postgres;

--
-- Name: COLUMN casas_info.tipo; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN casas_info.tipo IS '1 for casa
0 for apto';


--
-- Name: casas_info_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE casas_info_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE casas_info_id_seq OWNER TO postgres;

--
-- Name: casas_info_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE casas_info_id_seq OWNED BY casas_info.id;


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY casas_fotos ALTER COLUMN id SET DEFAULT nextval('casas_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY casas_info ALTER COLUMN id SET DEFAULT nextval('casas_info_id_seq'::regclass);


--
-- Data for Name: auth_user; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY auth_user (id, first_name, last_name, email, password, reset_password_key, cedula, tel_cel) FROM stdin;
325	Yonaldy	Marrero	yonalmarr@gmail.com	6135ba88606f0b7b8e7eae2027d01b4d	\N	18557252	04242912828
\.


--
-- Data for Name: casas_fotos; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY casas_fotos (id, archivo, id_casa) FROM stdin;
\.


--
-- Name: casas_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('casas_id_seq', 292, true);


--
-- Data for Name: casas_info; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY casas_info (id, nombre_carpeta, habitacion, banos, estacionamiento, area_verde, cocina, documentos, descripcion, id_user, vendida, tipo, estado, zona) FROM stdin;
\.


--
-- Name: casas_info_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('casas_info_id_seq', 67, true);


--
-- Name: auth_user_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY auth_user
    ADD CONSTRAINT auth_user_pkey PRIMARY KEY (id);


--
-- Name: casas_info_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY casas_info
    ADD CONSTRAINT casas_info_pkey PRIMARY KEY (id);


--
-- Name: pk_casas_fotos; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY casas_fotos
    ADD CONSTRAINT pk_casas_fotos PRIMARY KEY (id);


--
-- Name: fk_casas_user; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY casas_info
    ADD CONSTRAINT fk_casas_user FOREIGN KEY (id_user) REFERENCES auth_user(id);


--
-- Name: fk_casasinfo_casasfotos; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY casas_fotos
    ADD CONSTRAINT fk_casasinfo_casasfotos FOREIGN KEY (id_casa) REFERENCES casas_info(id);


--
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- PostgreSQL database dump complete
--

