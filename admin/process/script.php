
<script type="text/javascript" src="../js/jquery.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

<script type="text/javascript" src="../js/materialize.js"></script>
<script type="text/javascript" src="../js/init.js"></script>
<script type="text/javascript" src="../js/sweetalert.min.js"></script>

<script type="text/javascript">

posicionarMenu();

$(window).scroll(function() {
    posicionarMenu();
});

function posicionarMenu() {
    var altura_del_header = $('.nav_pag').outerHeight(true);
    var altura_del_menu = $('.nav_pag2').outerHeight(true);

    var altura_del_portf = $('#portfoliolist').outerHeight(true);
    var altura_del_submenu = $('#submenu').outerHeight(true);



    if ($(window).scrollTop() >= altura_del_header){
        $('.nav_pag2').addClass('fixed9');
        $('#lema').removeClass('center');
        $('#lema').addClass('right');
        $('#icono').show();
        $('.wrapperx').css('margin-top', (altura_del_menu) + 'px');

      //  $('#submenu').css('position','fixed');
        $('#submenu').addClass('fixed8');
        $('#portfoliolist').css('margin-top',(altura_del_menu) + 'px');
        //$('#filters').css('margin-top', (altura_del_menu) + 'px');
      //  $('#submenu').css('position','fixed');

    } else {
        $('.nav_pag2').removeClass('fixed9');
        $('#lema').removeClass('right');
        $('#lema').addClass('center');
        $('#icono').css('display','none');

        $('.wrapperx').css('margin-top', '0');
      //  $('#submenu').css('position','relative');
        $('#submenu').removeClass('fixed8');
        $('#portfoliolist').css('margin-top',0);
    }
}



</script>
