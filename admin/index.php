<!DOCTYPE html>
<html>
  <head>
    <?php include "process/head.php" ?>
  </head>
  <body style="background-color:#828AB8">

<?php include "process/nav.php" ?>
<div class="row wrapperx" style="margin-top:10px;margin-bottom:10px;z-index:1;background-color:#e2e2e9;">
<div class="container">
  <div class="row"></div>
  <div class="row">
      <form class="col s12" action="process/enviado.php" method="post" enctype="multipart/form-data">

        <div class="row">
          <div class="input-field col s6">
            <select name="estado" id="estado">
              <option value="" disabled selected>Seleccione un Estado</option>
              <option value="Aragua">Aragua</option>
          		<option value="Carabobo">Carabobo</option>
            </select>
            <label>Estado</label>
          </div>
          <div class="input-field col s6">
            <select name="tipo" id="tipo">
              <option value="" disabled selected>Seleccione una Opción</option>
              <option value="0">Apartamento</option>
              <option value="1">Casa</option>
            </select>
            <label>Tipo</label>
          </div>
        </div>
        <div class="row">
          <div class="input-field col s3">
            <input name="zona" id="zona" type="text" min="0" class="validate">
            <label for="zona">Zona</label>
          </div>
          <div class="input-field col s3">
            <input name="casa" id="casa" type="text" min="0" class="validate">
            <label for="casa">Referencia</label>
          </div>
          <div class="input-field col s3">
            <input name="habitacion" id="habitacion" type="number" min="0" class="validate">
            <label for="habitacion">Habitaciones</label>
          </div>
          <div class="input-field col s3">
            <input name="banos" id="banos" type="number" min="0" class="validate">
            <label for="banos">Ba&ntilde;os</label>
          </div>
        </div>
        <div class="row">
          <div class="input-field col s3">
            <input type="checkbox" name="estacionamiento" id="estacionamiento" />
            <label for="estacionamiento">Estacionamiento</label>
          </div>
          <div class="input-field col s3">
            <input type="checkbox" name="area_verde" id="area_verde" />
            <label for="area_verde">Area Verde</label>
          </div>
          <div class="input-field col s3">
            <input type="checkbox" name="cocina" id="cocina" />
            <label for="cocina">Cocina</label>
          </div>
          <div class="input-field col s3">
            <input type="checkbox" name="documentos" id="documentos" />
            <label for="documentos">Documentos en Regla</label>
          </div>
        </div>
        <div class="row">
          <div class="col s12">
            <div class="input-field col s12">
              <textarea name="descripcion" id="descripcion" class="materialize-textarea"></textarea>
              <label for="descripcion">Descripcion</label>
            </div>
          </div>
        </div>

        <input type="file" name="archivo[]" id="archivoImage" multiple><br><br>
        <input type="submit" value="Guardar" name="submit">

      </form>

    </div>

</div>
</div>
<?php include "process/footer.php" ?>

    </body>
          <!--Import jQuery before materialize.js-->
    <?php include "process/script.php" ?>
    <script type="text/javascript">

    posicionarMenu();

    $(window).scroll(function() {
        posicionarMenu();
    });

    function posicionarMenu() {
        var altura_del_header = $('.nav_pag').outerHeight(true);
        var altura_del_menu = $('.nav_pag2').outerHeight(true);

        if ($(window).scrollTop() >= altura_del_header){
            $('.nav_pag2').addClass('fixed9');
            $('#lema').removeClass('center');
            $('#lema').addClass('right');
            $('#icono').show();

            $('.wrapperx').css('margin-top', (altura_del_menu) + 'px');

        } else {
            $('.nav_pag2').removeClass('fixed9');
            $('#lema').removeClass('right');
            $('#lema').addClass('center');
            $('#icono').css('display','none');

            $('.wrapperx').css('margin-top', '0');

        }
    }

function guardar(){
  /*estado = $('#estado').val();
  tipo = $('#tipo').val();
  zona = $('#zona').val();
  casa = $('#casa').val();
  habitacion = $('#habitacion').val();
  banos = $('#banos').val();
  estacionamiento = document.getElementById("estacionamiento").checked;
  area_verde = document.getElementById("area_verde").checked;
  cocina = document.getElementById("cocina").checked;
  documentos = document.getElementById("documentos").checked;
  descripcion =  $('#descripcion').val();
*/



  var inputFileImage = document.getElementById("archivoImage");
  var file = inputFileImage.files[0];
  var data = new FormData();
  data.append('archivo',file);
  var url = "enviado.php";
  $.ajax({
  url:url,
  type:'POST',
  contentType:false,
  data:data,
  processData:false,
  cache:false});
  /*$.post( "process/enviado.php",
  {
    estado:estado,
    tipo:tipo,
    zona:zona,
    casa :casa,
    habitacion :habitacion,
    banos:banos ,
    estacionamiento:estacionamiento,
    area_verde:area_verde,
    cocina:cocina,
    documentos:documentos,
    descripcion:descripcion,
file:file,

  }
  ,function(data){
    alert(data);
  });
*/
}

    </script>

  </html>
