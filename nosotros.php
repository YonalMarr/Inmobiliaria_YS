<!DOCTYPE html>
<html>
  <head>
    <?php include "process/head.php" ?>
  </head>
  <body style="background-color:#828AB8">

<?php include "process/nav.php" ?>

<div class="row wrapperx" style="margin-top:10px;margin-bottom:10px;z-index:1;background-color:#e2e2e9">
<div class="container">

  <div class="col s12 m4 l4">
    <div class="card">
      <div class="card-image">
        <img src="img/about.jpg">
        <span class="card-title ">¿QUIENES SOMOS?</span>
      </div>
      <div class="card-content">
        <p>Somos un equipo de profesionales capacitados y orientados para realizar el sueño de su familia y así satisfacer todas las necesidades en el aspecto inmobiliario. Realizamos todo lo relacionado con bienes raíces, administración de todo tipo de bines inmuebles, tramitación de solvencias, redacción de documentos legales, opción compra-venta, declaración jurada, declaración de bienhechurías, cesiones, poderes, títulos supletorios, entre otros.</p>
      </div>
    </div>
</div>
  <div class="col s12 m4 l4">
    <div class="card">
      <div class="card-image">
        <img src="img/about.jpg">
        <span class="card-title">MISIÓN</span>
      </div>
      <div class="card-content">
        <p>Nuestra misión es proveer servicios inmobiliarios con altos niveles de confiabilidad, responsabilidad, seriedad y flexibilidad. Nuestros sólidos principios éticos, fuerte compromiso, eficacia y honestidad nos han guiado desde hace mas de 15 años; por ello es que la Inmobiliaria Yelitza Sueños, C.A. apoya constantemente a su cartera de clientes porque nos involucran a todos como sociedad y personas, ayudándolos a cumplir “Su Casa un Sueño Hecho Realidad”.</p>
      </div>
    </div>
</div>
  <div class="col s12 m4 l4">
    <div class="card">
      <div class="card-image">
        <img src="img/about.jpg">
        <span class="card-title">VISIÓN</span>
      </div>
      <div class="card-content">
        <p>Transformarnos en una empresa líder en el mercado de inversiones, principalmente inmobiliario, ampliando nuestros servicios y negocios y así lograr los mejores resultados a nuestros clientes, actuando siempre con integridad en cada operación. Ofreciendo siempre un servicio profesional y personalizado, orientado a cuidar el patrimonio de nuestros clientes y lograr nuestro desarrollo como empresa para así satisfacer las necesidades de nuestros clientes al mismo tiempo que impulsamos nuestro crecimiento como inmobiliaria a nivel regional.</p>
      </div>
    </div>
</div>
<div class="col s12 m12 l12">
  <div class="card">
    <div class="card-image">
      <img src="img/about.jpg">
      <span class="card-title">OBJETIVOS</span>
    </div>
    <div class="card-content">
      <p>  ✓ Nuestro principal objetivo es alcanzar y mantener los mas altos estándares de satisfacción al cliente en el sector inmobiliario, a través de nuestros inmuebles y servicios innovadores.</p>
      <p>  ✓ Asegurar una fuerte posición competitiva en nuestros mercados relevantes, a través de una oferta creativa de inmuebles y excelencia operacional.</p>
      <p>  ✓ Buscamos aliarnos con las mejores inmobiliarias y constructoras del Pais, entregando valor agregado tanto para la empresa como para nuestros clientes.</p>
      <p>  ✓ Ser reconocidos como empleadores de primer nivel.</p>
      <p>  ✓ Demostrar nuestro compromiso con el desarrollo sustentable y jugar un rol preponderante en la responsabilidad social dentro de nuestro circulo de influencias.</p>
      <p>  ✓ Tener un desempeño financiero a largo plazo y se la organización mas recomendada en nuestra industria.</p>
    </div>
  </div>
</div>

</div>

</div>
</div>
<?php include "process/footer.php" ?>
      <!--Import jQuery before materialize.js-->
<?php include "process/script.php" ?>

<script type="text/javascript">

posicionarMenu();

$(window).scroll(function() {
    posicionarMenu();
});

function posicionarMenu() {
    var altura_del_header = $('.nav_pag').outerHeight(true);
    var altura_del_menu = $('.nav_pag2').outerHeight(true);

    if ($(window).scrollTop() >= altura_del_header){
        $('.nav_pag2').addClass('fixed9');
        $('#lema').removeClass('center');
        $('#lema').addClass('right');
        $('#icono').show();

        $('.wrapperx').css('margin-top', (altura_del_menu) + 'px');

    } else {
        $('.nav_pag2').removeClass('fixed9');
        $('#lema').removeClass('right');
        $('#lema').addClass('center');
        $('#icono').css('display','none');

        $('.wrapperx').css('margin-top', '0');

    }
}


</script>

    </body>
  </html>
