<!DOCTYPE html>
<html>
  <head>
    <?php include "process/head.php" ?>
  </head>
  <body style="background-color:#828AB8">

<?php include "process/nav.php" ?>

<div class="row wrapperx" style="margin-top:10px;margin-bottom:10px;z-index:1;background-color:#e2e2e9;">
<div class="container">

        <div class="col s12 m4 l4">
          <div class="card" style="border-radius:25px">
            <div class="card-image" >
              <img src="img/slider98.png">
              <span class="card-title">Bienvenidos</span>
            </div>
            <div class="card-content">
              <p>La Inmobiliaria Yelitza Sueños C.A. tiene un equipo de profesionales capacitados y orientados para realizar el sueño de su familia.</p>
            </div>
          </div>
      </div>
        <div class="col s12 m4 l4">
          <div class="card" style="border-radius:25px">
            <div class="card-image" >
              <img src="img/slider97.png">
              <span class="card-title">Su Mejor Solución</span>
            </div>
            <div class="card-content">
              <p>Realizamos todo lo relacionado con bienes raíces, tramitación de solvencias, redacción de documentos legales, opción compra-venta, declaración jurada, entre otros.</p>
            </div>
          </div>
      </div>
        <div class="col s12 m4 l4">
          <div class="card" style="border-radius:25px">
            <div class="card-image" >
              <img src="img/slider99.png">
              <span class="card-title">Experiencia</span>
            </div>
            <div class="card-content">
              <p>Nuestros sólidos principios éticos, fuerte compromiso, eficacia y honestidad nos han guiado desde hace mas de 15 años en el ramo inmobiliario.</p>
            </div>
          </div>
      </div>
</div>
</div>
<?php include "process/footer.php" ?>

    </body>
          <!--Import jQuery before materialize.js-->
    <?php include "process/script.php" ?>

    <script type="text/javascript">

    posicionarMenu();

    $(window).scroll(function() {
        posicionarMenu();
    });

    function posicionarMenu() {
        var altura_del_header = $('.nav_pag').outerHeight(true);
        var altura_del_menu = $('.nav_pag2').outerHeight(true);

        if ($(window).scrollTop() >= altura_del_header){
            $('.nav_pag2').addClass('fixed9');
            $('#lema').removeClass('center');
            $('#lema').addClass('right');
            $('#icono').show();

            $('.wrapperx').css('margin-top', (altura_del_menu) + 'px');

        } else {
            $('.nav_pag2').removeClass('fixed9');
            $('#lema').removeClass('right');
            $('#lema').addClass('center');
            $('#icono').css('display','none');

            $('.wrapperx').css('margin-top', '0');

        }
    }


    </script>

  </html>
