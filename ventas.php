<?php
include "process/connect.php";
?>
<!DOCTYPE html>
<html>
	<head>
	  <?php include "process/head.php" ?>
	  <link rel="stylesheet" href="css/layout.css">
		<style>
	         /* jssor slider bullet navigator skin 05 css */
	         /*
	         .jssorb05 div           (normal)
	         .jssorb05 div:hover     (normal mouseover)
	         .jssorb05 .av           (active)
	         .jssorb05 .av:hover     (active mouseover)
	         .jssorb05 .dn           (mousedown)
	         */
	         .jssorb05 {
	             position: absolute;
	         }
	         .jssorb05 div, .jssorb05 div:hover, .jssorb05 .av {
	             position: absolute;
	             /* size of bullet elment */
	             width: 16px;
	             height: 16px;
	             background: url('img/b05.png') no-repeat;
	             overflow: hidden;
	             cursor: pointer;
	         }
	         .jssorb05 div { background-position: -7px -7px; }
	         .jssorb05 div:hover, .jssorb05 .av:hover { background-position: -37px -7px; }
	         .jssorb05 .av { background-position: -67px -7px; }
	         .jssorb05 .dn, .jssorb05 .dn:hover { background-position: -97px -7px; }

	         /* jssor slider arrow navigator skin 12 css */
	         /*
	         .jssora12l                  (normal)
	         .jssora12r                  (normal)
	         .jssora12l:hover            (normal mouseover)
	         .jssora12r:hover            (normal mouseover)
	         .jssora12l.jssora12ldn      (mousedown)
	         .jssora12r.jssora12rdn      (mousedown)
	         */
	         .jssora12l, .jssora12r {
	             display: block;
	             position: absolute;
	             /* size of arrow element */
	             width: 30px;
	             height: 46px;
	             cursor: pointer;
	             background: url('img/a12.png') no-repeat;
	             overflow: hidden;
	         }
	         .jssora12l { background-position: -16px -37px; }
	         .jssora12r { background-position: -75px -37px; }
	         .jssora12l:hover { background-position: -136px -37px; }
	         .jssora12r:hover { background-position: -195px -37px; }
	         .jssora12l.jssora12ldn { background-position: -256px -37px; }
	         .jssora12r.jssora12rdn { background-position: -315px -37px; }
	     </style>
			 <script type="text/javascript">

			 		jssor_1_slider_init = function(jssor) {

			 				var jssor_SlideshowTransitions = [
			 					{$Duration:1200,$Opacity:2}
			 				];

			 				var jssor_options = {
			 					$AutoPlay: 1,
			 					$SlideshowOptions: {
			 						$Class: $JssorSlideshowRunner$,
			 						$Transitions: jssor_SlideshowTransitions,
			 						$TransitionsOrder: 1
			 					},
			 					$ArrowNavigatorOptions: {
			 						$Class: $JssorArrowNavigator$
			 					},
			 					$BulletNavigatorOptions: {
			 						$Class: $JssorBulletNavigator$
			 					}
			 				};

			 				var jssor_1_slider = new $JssorSlider$(jssor, jssor_options);

			 				/*responsive code begin*/
			 				/*remove responsive code if you don't want the slider scales while window resizing*/
			 				function ScaleSlider() {
			 						var refSize = jssor_1_slider.$Elmt.parentNode.clientWidth;
			 						if (refSize) {
			 								refSize = Math.min(refSize, 600);
			 								jssor_1_slider.$ScaleWidth(refSize);
			 						}
			 						else {
			 								window.setTimeout(ScaleSlider, 30);
			 						}
			 				}
			 				ScaleSlider();
			 				$Jssor$.$AddEvent(window, "load", ScaleSlider);
			 				$Jssor$.$AddEvent(window, "resize", ScaleSlider);
			 				$Jssor$.$AddEvent(window, "orientationchange", ScaleSlider);
			 				/*responsive code end*/
			 		};

			 		//total = document.getElementById("total").innerHTML;
			 		//jssor_1_slider_init('jssor_1');
			 		//jssor_1_slider_init('jssor_2');
			 		//jssor_1_slider_init('jssor_3');
			 		//jssor_1_slider_init('jssor_4');
			 </script>
	</head>
	<body style="background-color:#828AB8">
		<?php include "process/nav.php" ?>

		<div class="row wrapperx" style="margin-top:10px;margin-bottom:10px;z-index:1;background-color:#e2e2e9">

<div id="submenu" style="width=100%">
		<ul id="filters" class="clearfix" style="position:relative;">
			<li><span class="filter active" data-filter=".Carabobo, .Aragua, .casa, .apto">Todas</span></li>
			<li><span class="filter" data-filter=".Carabobo">Carabobo</span></li>
			<li><span class="filter" data-filter=".Aragua">Aragua</span></li>
			<li>\</li>
			<li><span class="filter" data-filter=".casa">Casas</span></li>
			<li><span class="filter" data-filter=".apto">Apartamentos</span></li>
		</ul>
	</div>
		<div id="portfoliolist" >
		<?php
	/*		$sql9="select count(a.*) as total from casas_info a";
			$result9 = pg_query($sql9);
			$row9 = pg_fetch_assoc($result9);
			$echo .= "<input type=\"text\" id=\"total\" name=\"total\" value=\"".$row9['total']."\" >";
*/

			$sql = "select a.*, a.nombre_carpeta ||'/'||b.archivo as archivo from casas_info a, casas_fotos b where a.vendida = FALSE and b.archivo like '0.%' and a.id = b.id_casa";
			$result = pg_query($sql);
			$i = 1;
			while ($row = pg_fetch_assoc($result)){
				if($row['tipo'] == 1){$tipo = 'casa';}else{$tipo = 'apto'; }
				if($row['estacionamiento'] == 't'){$estacionamiento = 'SI';}else{$estacionamiento = 'NO'; }
				if($row['area_verde'] == 't'){$area_verde = 'SI';}else{$area_verde = 'NO'; }
				if($row['cocina'] == 't'){$cocina = 'SI';}else{$cocina = 'NO'; }
				if($row['documentos'] == 't'){$documentos = 'SI';}else{$documentos = 'NO'; }
				$echo .="<div class=\"portfolio ".$tipo." ".$row['estado']."\">
							<div class=\"portfolio-wrapper\">
							<a href=\"#modal".$i."\"><img style=\"border-radius: 35px 35px 0px 0px;\" src=\"casas/".$row['archivo']."\" alt=\"\" /></a>
								<div class=\"label\">
									<div class=\"label-text\">
										<a class=\"text-title\" style=\"color: #CCCCFF;font-weight: bold;\">".strtoupper($row['zona'])."</a>
									</div>
									<div style=\"border-radius: 0px 0px 35px 35px;\" class=\"label-bg\" ></div>
								</div>
							</div>
						</div>";

						$echo .= "<div id=\"modal_info".$i."\" class=\"modal\"  style=\"background:#e2e2e9;border-radius:50px;width:550px\">
						<div class=\"modal-content\">
						<h3 style=\"text-align: center;color:#3F51B5;text-transform:uppercase\">".$row['estado']." - ".$row['zona']."</h3>
						</br>
						<table border=\"0\">
						<tr><td style=\"text-align: center;\" width=\"10%\">Habitaciones: ".$row['habitacion']."</td>
						<td style=\"text-align: center;\" width=\"10%\">Baños: ".$row['banos']."</td></tr>
						<tr><td style=\"text-align: center;\" width=\"10%\">Estacionamiento: ".$estacionamiento."</td>
						<td style=\"text-align: center;\" width=\"10%\">Area Verde: ".$area_verde."</td></tr>
						<tr><td style=\"text-align: center;\" width=\"10%\">Cocina: ".$cocina."</td>
						<td style=\"text-align: center;\" width=\"10%\">Documentos en Regla: ".$documentos."</td></tr>
						</table>
						</br>
						<div style=\"text-align: center;\">".$row['descripcion']."</div>

						</div>

				</div>";


					$echo .= "<div id=\"modal".$i."\" class=\"modal\" style=\"background:#e2e2e9;border-radius:35px\">
								<div class=\"modal-content\" >
							    	<h4 style=\"color:#3F51B5\">".strtoupper($row['zona'])."</h4>

										<div id=\"jssor_".$i."\" style=\"position:relative;margin:0 auto;top:0px;left:0px;width:600px;height:500px;overflow:hidden;visibility:hidden;\">
										        <div data-u=\"loading\" style=\"position:absolute;top:0px;left:0px;background-color:rgba(0,0,0,0.7);\">
										            <div style=\"filter: alpha(opacity=70); opacity: 0.7; position: absolute; display: block; top: 0px; left: 0px; width: 100%; height: 100%;\"></div>
										            <div style=\"position:absolute;display:block;background:url('img/loading.gif') no-repeat center center;top:0px;left:0px;width:100%;height:100%;\"></div>
										        </div>
										        <div data-u=\"slides\" style=\"cursor:default;position:relative;top:0px;left:0px;width:600px;height:500px;overflow:hidden;\">";

														$sql2 = "select 'casas/'||b.nombre_carpeta||'/'||a.archivo as foto from casas_fotos a, casas_info b where b.id = a.id_casa and b.id =".$row['id'];
														$result2 = pg_query($sql2);
														while ($row2 = pg_fetch_assoc($result2)){
															$echo .= "<div>
																					<img style=\"border-radius:35px\" data-u=\"image\" src=\"".$row2['foto']."\" />
																				</div>";
														}


										        $echo .= "</div>
										        <div data-u=\"navigator\" class=\"jssorb05\" style=\"bottom:16px;right:16px;\" data-autocenter=\"1\">
										            <div data-u=\"prototype\" style=\"width:16px;height:16px;\"></div>
										        </div>
										        <span data-u=\"arrowleft\" class=\"jssora12l\" style=\"top:0px;left:0px;width:30px;height:46px;\" data-autocenter=\"2\"></span>
										        <span data-u=\"arrowright\" class=\"jssora12r\" style=\"top:0px;right:0px;width:30px;height:46px;\" data-autocenter=\"2\"></span>
										    </div>

									</div>
			    				<div class=\"modal-footer\" style=\"background:#e2e2e9\">
										<a href=\"#\" class=\"modal-action modal-close waves-effect waves-green btn-flat\">Cerrar</a>
			    					<a href=\"#modal_info".$i."\" class=\"modal-action waves-effect waves-green btn-flat\">Información</a>
			    				</div>
							</div>";

							$echo_init .= "jssor_1_slider_init('jssor_".$i."');";

						$i++;
			}
		echo $echo;
		?>


	</div>



		</div>
		<?php include "process/footer.php" ?>
		<?php include "process/script.php" ?>
		<script type="text/javascript" src="js/jquery.mixitup.min.js"></script>
		<script src="js/jssor.slider-23.1.5.min.js" type="text/javascript"></script>

		<script type="text/javascript">
		$(function () {

			var filterList = {
				init: function () {
					$('#portfoliolist').mixItUp({
						selectors: {
							target: '.portfolio',
							filter: '.filter'
						},
						load: {
							filter: '.carabobo, .aragua, .casa, .apto'
						}
					});
				}
			};
			filterList.init();
		});
		</script>
<?php 		echo "<script type=\"text/javascript\">".$echo_init."</script>"; ?>
	</body>
</html>
