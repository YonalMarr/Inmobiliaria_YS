<!DOCTYPE html>
<html>
  <head>
    <?php include "process/head.php" ?>
  </head>
  <body style="background-color:#828AB8">

<?php include "process/nav.php" ?>

<div class="row wrapperx" style="margin-top:10px;margin-bottom:10px;z-index:1;background-color:#e2e2e9">
<div class="container">

        <div class="col s12 m4 l4">
          <a href="https://www.facebook.com/profile.php?id=100009570318641" target="_blank">

          <div class="card" style="border-radius:25px">
            <div class="card-image"  style="background-color: rgb(226, 226, 233);">
              <img src="img/Facebook.png">
              <span class="card-title"></span>
            </div>
            <div class="card-content">
              <p>Inmobiliaria Yelitza Sueños</p>
            </div>
          </div>
        </a>
      </div>
        <div class="col s12 m4 l4">
          <a href="https://instagram.com/inmobiliariayelitza/" target="_blank">

          <div class="card" style="border-radius:25px">
            <div class="card-image"  style="background-color: rgb(226, 226, 233);">
              <img src="img/Instagram.png">
              <span class="card-title"></span>
            </div>
            <div class="card-content">
              <p>inmobiliariayelitza</p>
            </div>
          </div>
        </a>

      </div>
        <div class="col s12 m4 l4">
          <div class="card" style="border-radius:25px">
            <div class="card-image"  style="background-color: rgb(226, 226, 233);">
              <img src="img/Twitter.png">
              <span class="card-title"></span>
            </div>
            <div class="card-content">
              <p>Proximamente</p>
            </div>
          </div>
      </div>

      <div class="col s12 m12 l12">
        <div class="card" style="border-radius:25px">
          <div class="card-content">
            <h1 >Contactanos</h1>
            <p>Envianos tus dudas, consultas y preguntas por aquí:</p></br>
            <div class="row">
              <form id="form_account" method="post" class="col s12">
                <div class="row">
                  <div class="input-field col s4">
                    <input id="userName" type="text" class="validate " >
                    <label for="userName">Nombre</label>
                  </div>
                  <div class="input-field col s4">
                   <input id="userEmail" type="text" class="validate">
                   <label for="userEmail">Correo Electronico</label>
                  </div>
                  <div class="input-field col s4">
                    <input id="userPhone" type="text" class="validate">
                    <label for="userPhone">Telefono</label>
                  </div>
                </div>
                <div class="row">
                  <div class="input-field col s12">
                    <textarea id="userMsg" class="materialize-textarea"></textarea>
                    <label for="userMsg">Asunto</label>
                  </div>
                </div>
                <div class="row">
                  <div class="col offset-s5 s8">

                  <a style="background-color:#3F51B5;color:#ccccff" class="waves-effect waves-light btn" onclick="enviar_correo()">Enviar</a>
                  </div>
          			</div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
</div>
<?php include "process/footer.php" ?>
      <!--Import jQuery before materialize.js-->
<?php include "process/script.php" ?>
<script type="text/javascript">
function enviar_correo(){
  var userName = $('#userName').val();
  var userEmail = $('#userEmail').val();
  var userPhone = $('#userPhone').val();
  var userMsg = $('#userMsg').val();

if(
//  userEmail == "" || userName == "" || userPhone=="" || userMsg==""
userName == ""
){

  alert('Por favor completar todos los campos');
  return false;
}else{
  $.post('process/enviado.php?val='+userName+';'+userEmail+';'+userPhone+';'+userMsg,
  function(data){
    if(data==1){
      swal({
        title: '¡Correo enviado con Exito!',
        text: 'Pronto nuestro equipo se estara contactando con usted.',
        type: "success",
        showCancelButton: false,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Ok!",
        closeOnConfirm: false,
        closeOnCancel: false
      },
      function(isConfirm){
        if (isConfirm) {
        window.location.href = 'contactos.php';
        }
      });

    }else{
      alert('error');
      /*
      swal({
        title: "Error!",
        text: "Hubo un error a la hora de guardar!",
        imageUrl: "img/thumbs-down.jpg",
        imageSize:"190x150"      });
    */
    }
  });

}

}
</script>


    </body>
  </html>
